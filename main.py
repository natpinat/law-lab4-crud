from typing import Optional
from fastapi import FastAPI, File, UploadFile
from fastapi.security import HTTPBasic
from pydantic import BaseModel
import os

security = HTTPBasic()
app = FastAPI()

data = {}
count = {"count":0}

class Message(BaseModel) :
    title: Optional[str] = None
    description: Optional[str] = None
    author: Optional[str] = None

class update_message(BaseModel) :
   title: Optional[str] = None
   description: Optional[str] = None


@app.post('/message/')
def create_message (message : Message) :
    data[count["count"]] = message
    count["count"]+=1 
    return message

@app.get("/message/{id}",response_model=Message)
def get_message(id: int) :
    message = data[id]
    return message

@app.put('/message/{id}', response_model=Message)
async def edit_message(id: int, message : update_message) :
    get = data[id]
    get.title = message.title
    get.description = message.description
    return get

@app.delete('/message/{id}')
def delete_message(id:int) :
    del data[id]
    return {"status" : "deleted"}

@app.post('/upload-message/')
async def upload_message(file: UploadFile = File(...)) :
    fullpath = os.path.join("file/", file.filename)
    print(fullpath)
    await file.seek(0)
    try :
        with open(fullpath, "wb") as buffer:
            while True:
                contents = await file.read()
                if not contents:
                    break
                buffer.write(contents)        
        processed_content = "file received"
    except Exception :
        contents = "could not process file"
        processed_content = contents
    return {"filename": file.filename, "contents":processed_content}
